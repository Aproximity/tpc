#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <time.h>
#include "serveur.h"

int extraitFichier(char *requete, char *tableauNomFichier, int tailleTableauNomFichier) {
    /* QUESTION 8.a
    * Exemple de requette: 
    * GET /index.html HTTP/1.1\r\nHost:www.milliondollarhomepage.com\r\n\r\n
    * 
    * Ce que l'on doit extraire : 
    *  /index.html
    * 
    * On doit chercher le premier "/" pour ensuite extaire tout ce qui suit jusqu'a rencontrer un espace
    */

    char *buf = requete;
    int ret = 0;
    buf = strchr(buf,'/');
    if (buf == NULL) {
        goto on_error;
    }
    buf++;
    if(*buf==' '){
        strncpy(tableauNomFichier,"index.html",11); // on utilise strncpy et non pas strcpy pour protéger des failles de sécurite
        return ret;
    }else{
        char *save = buf;
        buf = strchr(save,' ');
        if(buf == NULL){
            goto on_error;
        }
        size_t taille = (buf-save)*sizeof(char);
        strncpy(tableauNomFichier,save,taille);
        printf("%s", tableauNomFichier);
        tableauNomFichier[taille]='\0';
    }
    return ret;
    on_error:
    ret = 1;
    perror("Mauvais formatage de la requete");
    return ret;

}

int longeur_fichier(char *nomFichier) {
    /* QUESTION 8.b
    * Determiner la taille d'un fichier passer en parametre
    */
    size_t resultat = -1;
    FILE *fichier = fopen(nomFichier, "r");
    if (fichier == NULL) {
        return resultat;
    }
    fseek(fichier, 0, SEEK_END);
    resultat = ftell(fichier);
    if (fclose(fichier) == EOF) {
        perror("Erreur lors de la fermeture du fichier");
        return resultat;
    }
    return resultat;
}

int envoyerContenuFichierTexte(char *nomFichier) {
    /* fonction envoyerContenuFichierTexte(char *nomFichier)
    * Créé par Kévin LOPES le 01/12/2019, Question 8.c
    *
    * Rôle : Lit un fichier et le transmet au client via le protocole HTTP
    *
    * Paramètre:
    * IN :
    *   char *nomFichier : pointeur sur le nom de fichier
    *
    * OUT :
    *   int 0 : succès de l'envoi
    *   int -1 : erreur, le fichier n'a pas pu être ouvert
    *
    */
   printf("DEBUG 1");
    FILE *fichier;
    long taille = longeur_fichier(nomFichier);
    char *fileContent = malloc(taille);
    printf("DEBUG 2");

    //Ouverture du fichier en lecture seule
    fichier = fopen(nomFichier, "r");
    printf("DEBUG 3 %s\n", fichier);

    if (fichier == NULL) {
        perror("Erreur lors de l'ouverture du fichier");
        return -1;
    }
    // On place le curseur au debut du fichier
    fseek(fichier, 0, SEEK_SET);
    printf("DEBUG 4");

    //Lecture du fichier caracetre par caractere
    fread(fileContent, taille, 1, fichier);
    printf("DEBUG 5");
    fclose(fichier);
    printf("DEBUG 6 %s\n", fichier);
    
    //Envoie du fichier au client
    EmissionBinaire(fileContent, taille);
    printf("DEBUG 7\n");
    return 0;
}

int envoyerReponse200HTML(char *nomFichier) {
    /* fonction envoyerReponse200HTML(char *nomFichier)
    * Créé par Kévin LOPES le 01/12/2019, Question 8.d
    *
    * Rôle : Envoie le code de reponse HTTP 200 au client de type text/html
    *
    * Paramètre:
    * IN :
    *   char *nomFichier : pointeur sur le nom de fichier
    *
    * OUT :
    *   int 0 : succès de l'envoi
    *   int -1 : erreur, l'envoie n'a pas été éffectuer
    *
    * Exemple de ce qui faut envoyer :
    *   HTTP/1.1 200 OK
    *   Date : Sat, 15 Jan 2000 14:37:12 GMT Server : Microsoft-IIS/2.0
    *   Content-Type : text/HTML; charset=UTF-8
    *   Content-Length : 1245
    *
    */

    // Stockage de la taille du fichier a envoyer
    int size = longeur_fichier(nomFichier);
    // Emmission de la reponse 2OO OK HTTP
        // Test de la bonne Emission du message
        if (Emission("HTTP/1.1 200 OK\n") == -1) {
            perror("Erreur lors de l'envoie de la requête (Code Réponse)\n");
            return -1;
        }
        
    // Emission du type de contnue de la réponse, ici text/html
        // Test de la bonne Emission du message
        if (Emission("Content-Type: text/html; charset=UTF-8\n") == -1) {
            perror("Erreur lors de l'envoie de la requête (Content-Type)\n");
            return -1;
        }

    // Emission de la longeur de la reponse
        char contentLength[255];
        // Ajout de la taille du fichier a la reponse Content-Length, avec le double retour chariot pour terminer la réponse
        sprintf(contentLength, "Content-Length : %d\n\n", size);
        // Test de la bonne Emission du message
        if (Emission(contentLength) == -1) {
            perror("Erreur lors de l'envoie de la requête (Content-Lenght)\n");
            return -1;
        }
        return 0;
}

int envoyerReponse200CSS(char *nomFichier) {
    /* fonction envoyerReponse200CSS(char *nomFichier)
    * Créé par Kévin LOPES le 01/12/2019, Question 8.d
    *
    * Rôle : Envoie le code de reponse HTTP 200 au client de type text/html
    *
    * Paramètre:
    * IN :
    *   char *nomFichier : pointeur sur le nom de fichier
    *
    * OUT :
    *   int 0 : succès de l'envoi
    *   int -1 : erreur, l'envoie n'a pas été éffectuer
    *
    * Exemple de ce qui faut envoyer :
    *   HTTP/1.1 200 OK
    *   Date : Sat, 15 Jan 2000 14:37:12 GMT Server : Microsoft-IIS/2.0
    *   Content-Type : text/HTML; charset=UTF-8
    *   Content-Length : 1245
    *
    */

    // Stockage de la taille du fichier a envoyer
    int size = longeur_fichier(nomFichier);
    // Emmission de la reponse 2OO OK HTTP
        // Test de la bonne Emission du message
        if (Emission("HTTP/1.1 200 OK\n") == -1) {
            perror("Erreur lors de l'envoie de la requête (Code Réponse)\n");
            return -1;
        }
        
    // Emission du type de contnue de la réponse, ici text/css
        // Test de la bonne Emission du message
        if (Emission("Content-Type: text/css; charset=UTF-8\n") == -1) {
            perror("Erreur lors de l'envoie de la requête (Content-Type)\n");
            return -1;
        }

    // Emission de la longeur de la reponse
        char contentLength[255];
        // Ajout de la taille du fichier a la reponse Content-Length, avec le double retour chariot pour terminer la réponse
        sprintf(contentLength, "Content-Length : %d\n\n", size);
        // Test de la bonne Emission du message
        if (Emission(contentLength) == -1) {
            perror("Erreur lors de l'envoie de la requête (Content-Lenght)\n");
            return -1;
        }
        return 0;
}

int envoyerReponse404HTML(char *nomFichier) {
    /* fonction envoyerReponse404HTML(char *nomFichier)
    * Créé par Kévin LOPES le 01/12/2019, Question 8.e
    *
    * Rôle : Envoie le code de reponse HTTP 404 au client de type text/html ainsi que un message d'erreur dans le corp
    *
    * Paramètre:
    * IN :
    *   char *nomFichier : pointeur sur le nom de fichier
    *
    * OUT :
    *   int 0 : succès de l'envoi
    *   int -1 : erreur, l'envoie n'a pas été éffectuer
    *
    */

    // Page HTML que l'on va retourner
        printf("Nom Fichier 404 :%s\n", nomFichier);
        char message[500];
        sprintf(message, "<html><head></head><body><h1>404 Not Found</h1><pre>Le chemin : <b>%s</b> n'existe pas</pre></body></html>\n", nomFichier);
        // Stockage de la taille du fichier a envoyer
        int size = strlen(message);
    // Emmission de la reponse 404 Not Found HTTP
        // Test de la bonne Emission du message
        if (Emission("HTTP/1.1 404 NotFound\n") == 0) {
            perror("Erreur lors de l'envoie de la requête (Code Réponse)\n");
            return -1;
        }
    // Emission du type de contnue de la réponse, ici text/html
        // Test de la bonne Emission du message
        if (Emission("Content-Type: text/html; charset=UTF-8\n\n") == -1) {
            perror("Erreur lors de l'envoie de la requête (Content-Type)\n");
            return -1;
        }

    // Emission de la Page HTML d'erreur
        // Test de la bonne Emission du message
        if (Emission(message) == 0) {
            perror("Erreur lors de l'envoie de la requête (HTML-Content)\n");
            return -1;
        }
        return 0;
}

int envoyerReponse500HTML(char *nomFichier) {
    /* fonction envoyerReponse500HTML(char *nomFichier)
    * Créé par Kévin LOPES le 01/12/2019, Question 8.f
    *
    * Rôle : Envoie le code de reponse HTTP 500 au client de type text/html ainsi que un message d'erreur dans le corp
    *
    * Paramètre:
    * IN :
    *   char *nomFichier : pointeur sur le nom de fichier
    *
    * OUT :
    *   int 0 : succès de l'envoi
    *   int -1 : erreur, l'envoie n'a pas été éffectuer
    *
    */

    // Page HTML que l'on va retourner
        char message[100];
        sprintf(message, "Erreur Rencontée : %s\n", nomFichier);

    // Stockage de la taille du fichier a envoyer
        int size = strlen(message);
    // Emmission de la reponse 404 Not Found HTTP
        // Test de la bonne Emission du message
        if (Emission("HTTP/1.1 500 Server Error\n") == -1) {
            perror("Erreur lors de l'envoie de la requête (Code Réponse)\n");
            return -1;
        }    
    // Emission du type de contnue de la réponse, ici text/html
        // Test de la bonne Emission du message
        if (Emission("Content-Type: text/html; charset=UTF-8\n") == -1) {
            perror("Erreur lors de l'envoie de la requête (Content-Type)\n");
            return -1;
        }

    // Emission de la longeur de la reponse
        char contentLength;
        // Ajout de la taille du fichier a la reponse Content-Length, avec le double retour chariot pour terminer la réponse
        sprintf(contentLength, "Content-Length : %d\n\n", size);
        // Test de la bonne Emission du message
        if (Emission(contentLength) == -1) {
            perror("Erreur lors de l'envoie de la requête (Content-Lenght)\n");
            return -1;
        }

    // Emission de la Page HTML d'erreur
        // Test de la bonne Emission du message
        if (Emission(message) == -1) {
            perror("Erreur lors de l'envoie de la requête (HTML-Content)\n");
            return -1;
        }
        return 0;
}

int envoyerContenuFichierBinaire(char *nomFichier){
    /* fonction envoyerContenuFichierBinaire(char *nomFichier)
    * Créé par Kévin LOPES le 01/12/2019, Question 8.c
    *
    * Rôle : Lit un fichier et le transmet au client via le protocole HTTP
    *
    * Paramètre:
    * IN :
    *   char *nomFichier : pointeur sur le nom de fichier
    *
    * OUT :
    *   int 0 : succès de l'envoi
    *   int -1 : erreur, le fichier n'a pas pu être ouvert
    *
    */
   printf("DEBUG 1");
    FILE *fichier;
    long taille = longeur_fichier(nomFichier);
    char *fileContent = malloc(taille);
    printf("DEBUG 2");

    //Ouverture du fichier en lecture seule
    fichier = fopen(nomFichier, "rb");
    printf("DEBUG 3");

    if (fichier == NULL) {
        perror("Erreur lors de l'ouverture du fichier");
        return -1;
    }
    // On place le curseur au debut du fichier
    fseek(fichier, 0, SEEK_SET);
    printf("DEBUG 4");

    //Lecture du fichier caracetre par caractere
    fread(fileContent, 1, taille, fichier);
    printf("DEBUG 5");
    fclose(fichier);
    
    //Envoie du fichier au client
    EmissionBinaire(fileContent, taille);
    printf("DEBUG 6\n");
    return 0;
}

int envoyerReponse200JPG(char *nomFichier){
/* fonction envoyerReponse200JPG(char *nomFichier)
    * Créé par Kévin LOPES le 01/12/2019, Question 8.d
    *
    * Rôle : Envoie le code de reponse HTTP 200 au client de type image/jpeg
    *
    * Paramètre:
    * IN :
    *   char *nomFichier : pointeur sur le nom de fichier
    *
    * OUT :
    *   int 0 : succès de l'envoi
    *   int -1 : erreur, l'envoie n'a pas été éffectuer
    *
    * Exemple de ce qui faut envoyer :
    *   HTTP/1.1 200 OK
    *   Date : Sat, 15 Jan 2000 14:37:12 GMT Server : Microsoft-IIS/2.0
    *   Content-Type : text/HTML; charset=UTF-8
    *   Content-Length : 1245
    *
    */

    // Stockage de la taille du fichier a envoyer
    int size = longeur_fichier(nomFichier);
    // Emmission de la reponse 2OO OK HTTP
        // Test de la bonne Emission du message
        if (Emission("HTTP/1.1 200 OK\n") == -1) {
            perror("Erreur lors de l'envoie de la requête (Code Réponse)\n");
            return -1;
        }
        
    // Emission du type de contnue de la réponse, ici image/jpeg
        // Test de la bonne Emission du message
        if (Emission("Content-Type: image/jpg; charset=UTF-8\n") == -1) {
            perror("Erreur lors de l'envoie de la requête (Content-Type)\n");
            return -1;
        }

    // Emission de la longeur de la reponse
        char contentLength[255];
        // Ajout de la taille du fichier a la reponse Content-Length, avec le double retour chariot pour terminer la réponse
        sprintf(contentLength, "Content-Length : %d\n\n", size);
        // Test de la bonne Emission du message
        if (Emission(contentLength) == -1) {
            perror("Erreur lors de l'envoie de la requête (Content-Lenght)\n");
            return -1;
        }
        return 0;
}

int extractExtension(char *nomFichier, char *extension) {
    int ret = 0;
    if (nomFichier == NULL) {
        perror("Fichier NULL\n");
        return ret;
    }

    char *positionPoint = strrchr(nomFichier,'.');// Recherche du caractère '/', on sauvegarde tout ce qui suit dans le buffer
    positionPoint = positionPoint + 1;
    if (positionPoint == NULL) {
        perror("Erreur extension introuvable\n");
        ret = 0;
        return ret;
    }
    int tailleExtention = strlen(positionPoint);
    for (size_t i = 0; i < tailleExtention; i++) {
            extension[i] = positionPoint[i];
        }
    extension[tailleExtention] = '\0';
    printf("EXTENSION :%s\n", extension);
    ret = 1;
    return ret;
}

int envoyerReponse200JS(char *nomFichier){
/* fonction envoyerReponse200JPG(char *nomFichier)
    * Créé par Kévin LOPES le 01/12/2019, Question 8.d
    *
    * Rôle : Envoie le code de reponse HTTP 200 au client de type image/jpeg
    *
    * Paramètre:
    * IN :
    *   char *nomFichier : pointeur sur le nom de fichier
    *
    * OUT :
    *   int 0 : succès de l'envoi
    *   int -1 : erreur, l'envoie n'a pas été éffectuer
    *
    * Exemple de ce qui faut envoyer :
    *   HTTP/1.1 200 OK
    *   Date : Sat, 15 Jan 2000 14:37:12 GMT Server : Microsoft-IIS/2.0
    *   Content-Type : text/HTML; charset=UTF-8
    *   Content-Length : 1245
    *
    */

    // Stockage de la taille du fichier a envoyer
    int size = longeur_fichier(nomFichier);
    // Emmission de la reponse 2OO OK HTTP
        // Test de la bonne Emission du message
        if (Emission("HTTP/1.1 200 OK\n") == -1) {
            perror("Erreur lors de l'envoie de la requête (Code Réponse)\n");
            return -1;
        }
        
    // Emission du type de contnue de la réponse, ici application/javascript
        // Test de la bonne Emission du message
        if (Emission("Content-Type: application/javascript; charset=UTF-8\n") == -1) {
            perror("Erreur lors de l'envoie de la requête (Content-Type)\n");
            return -1;
        }

    // Emission de la longeur de la reponse
        char contentLength[255];
        // Ajout de la taille du fichier a la reponse Content-Length, avec le double retour chariot pour terminer la réponse
        sprintf(contentLength, "Content-Length : %d\n\n", size);
        // Test de la bonne Emission du message
        if (Emission(contentLength) == -1) {
            perror("Erreur lors de l'envoie de la requête (Content-Lenght)\n");
            return -1;
        }
        return 0;
}

int envoyerReponse200PNG(char *nomFichier){
/* fonction envoyerReponse200JPG(char *nomFichier)
    * Créé par Kévin LOPES le 01/12/2019, Question 8.d
    *
    * Rôle : Envoie le code de reponse HTTP 200 au client de type image/jpeg
    *
    * Paramètre:
    * IN :
    *   char *nomFichier : pointeur sur le nom de fichier
    *
    * OUT :
    *   int 0 : succès de l'envoi
    *   int -1 : erreur, l'envoie n'a pas été éffectuer
    *
    * Exemple de ce qui faut envoyer :
    *   HTTP/1.1 200 OK
    *   Date : Sat, 15 Jan 2000 14:37:12 GMT Server : Microsoft-IIS/2.0
    *   Content-Type : text/HTML; charset=UTF-8
    *   Content-Length : 1245
    *
    */

    // Stockage de la taille du fichier a envoyer
    int size = longeur_fichier(nomFichier);
    // Emmission de la reponse 2OO OK HTTP
        // Test de la bonne Emission du message
        if (Emission("HTTP/1.1 200 OK\n") == -1) {
            perror("Erreur lors de l'envoie de la requête (Code Réponse)\n");
            return -1;
        }
        
    // Emission du type de contnue de la réponse, ici image/png
        // Test de la bonne Emission du message
        if (Emission("Content-Type: image/png; charset=UTF-8\n") == -1) {
            perror("Erreur lors de l'envoie de la requête (Content-Type)\n");
            return -1;
        }

    // Emission de la longeur de la reponse
        char contentLength[255];
        // Ajout de la taille du fichier a la reponse Content-Length, avec le double retour chariot pour terminer la réponse
        sprintf(contentLength, "Content-Length : %d\n\n", size);
        // Test de la bonne Emission du message
        if (Emission(contentLength) == -1) {
            perror("Erreur lors de l'envoie de la requête (Content-Lenght)\n");
            return -1;
        }
        return 0;
}

int envoyerReponse200ICO(char *nomFichier){
/* fonction envoyerReponse200JPG(char *nomFichier)
    * Créé par Kévin LOPES le 01/12/2019, Question 8.d
    *
    * Rôle : Envoie le code de reponse HTTP 200 au client de type image/jpeg
    *
    * Paramètre:
    * IN :
    *   char *nomFichier : pointeur sur le nom de fichier
    *
    * OUT :
    *   int 0 : succès de l'envoi
    *   int -1 : erreur, l'envoie n'a pas été éffectuer
    *
    * Exemple de ce qui faut envoyer :
    *   HTTP/1.1 200 OK
    *   Date : Sat, 15 Jan 2000 14:37:12 GMT Server : Microsoft-IIS/2.0
    *   Content-Type : text/HTML; charset=UTF-8
    *   Content-Length : 1245
    *
    */

    // Stockage de la taille du fichier a envoyer
    int size = longeur_fichier(nomFichier);
    // Emmission de la reponse 2OO OK HTTP
        // Test de la bonne Emission du message
        if (Emission("HTTP/1.1 200 OK\n") == -1) {
            perror("Erreur lors de l'envoie de la requête (Code Réponse)\n");
            return -1;
        }
        
    // Emission du type de contnue de la réponse, ici image/x_icon
        // Test de la bonne Emission du message
        if (Emission("Content-Type: image/vnd.microsoft.icon; charset=UTF-8\n") == -1) {
            perror("Erreur lors de l'envoie de la requête (Content-Type)\n");
            return -1;
        }

    // Emission de la longeur de la reponse
        char contentLength[255];
        // Ajout de la taille du fichier a la reponse Content-Length, avec le double retour chariot pour terminer la réponse
        sprintf(contentLength, "Content-Length : %d\n\n", size);
        // Test de la bonne Emission du message
        if (Emission(contentLength) == -1) {
            perror("Erreur lors de l'envoie de la requête (Content-Lenght)\n");
            return -1;
        }
        return 0;
}