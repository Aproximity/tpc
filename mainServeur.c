#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "unistd.h"
#include "serveur.c"
#include "fonction.c"


int main() {
	char extension[10] = "html";
	char tableauNomFichier[255] = "TEST";
	char *message = NULL;
	Initialisation();

	while(1) {
		printf("TEST2\n");
		// Attente d'un client
		while(AttenteClient()==1) {
			printf("TEST3\n");
			// Lecture de la Requête
			message = Reception();
			// Si on recoit un message NULL de la part d'un client
			if(message == NULL) {
				// Si la requête est vide on affiche ce message d'erreur
				printf("Erreur, requête vide détectée.\n");
				// On cloture la connexion avec le client et on quitte la boucle de lecture de la requête
				// Cela permet de traiter plus rapidement une autre requête
				TerminaisonClient();
				continue;
			} else {
				printf("TEST4\n");
				// Verification que le type de requête est bien GET
				if (*(message) !='G' || *(message+1) != 'E' || *(message+2) != 'T' || *(message+3) != ' ') {
					printf("TEST5\n");
					// Si ce n'est pas une requête de type GET on retourne une erreur 500
					envoyerReponse500HTML("La requête ne peut être traitée.");
					// On cloture la connexion avec le client et on quitte la boucle de lecture de la requête
					// Cela permet de traiter plus rapidement une autre requête
					TerminaisonClient();
					continue;
				} else {
					printf("TEST6\n");
					//On extrait le nom du fichier demandé
					printf("Tableau AVANT : %s\n", tableauNomFichier);
					if (extraitFichier(message, tableauNomFichier, 255) == 1) {
						printf("Tableau APRES : %s\n", tableauNomFichier);
						printf("TEST7\n");
						// Si il n'y as pas de page demander ou qu'il y a une erreur
						envoyerReponse500HTML("Erreur lors du traitement de la requête.");
						// On cloture la connexion avec le client et on quitte la boucle de lecture de la requête
						// Cela permet de traiter plus rapidement une autre requête
						TerminaisonClient();
						continue;
					} else {
						printf("Tableau APRES : %s\n", tableauNomFichier);
						char path[1000] = "/home/lopes/Documents/tpc/templated-hielo/";
						printf("Page demandée : %s\n", tableauNomFichier);
						strcat(path,tableauNomFichier);
						printf("Chemin du fichier demandée : %s\n", path);
						// Vérification de l'existance du fichier demander sur le système
						if (access(path, F_OK) != 0) {
							printf("TEST9\n");
							// Le fichier n'est pas présent sur le systeme, on retourne donc une erreur 404
							envoyerReponse404HTML(tableauNomFichier);
						} else {
							extractExtension(path, extension);
							printf("EXTENSION MAIN :%s\n", extension);
							if (strcmp(extension, "html") == 0) {
								printf("TEST14\n");
								if (envoyerReponse200HTML(tableauNomFichier) == -1) {
									printf("TEST15\n");
									// On cloture la connexion avec le client et on quitte la boucle de lecture de la requête
									// Cela permet de traiter plus rapidement une autre requête
									TerminaisonClient();
									continue;
								} else {
									if(envoyerContenuFichierTexte(path) == -1){
										printf("TEST16\n");
										envoyerReponse500HTML("Erreur lors de l'envoi du fichier");
										// On cloture la connexion avec le client et on quitte la boucle de lecture de la requête
										// Cela permet de traiter plus rapidement une autre requête
										TerminaisonClient();
										continue;
									}
								}
								
							} else if (strcmp(extension, "css") == 0 || strcmp(extension, "map") == 0) {
								printf("TEST CSS MAIN 1\n");
								if (envoyerReponse200CSS(tableauNomFichier) == -1) {
									printf("TEST CSS MAIN 2\n");
									// On cloture la connexion avec le client et on quitte la boucle de lecture de la requête
									// Cela permet de traiter plus rapidement une autre requête
									TerminaisonClient();
									continue;
								} else {
									if(envoyerContenuFichierTexte(path) == -1){
										printf("TEST CSS MAIN 3\n");
										envoyerReponse500HTML("Erreur lors de l'envoi du fichier");
										// On cloture la connexion avec le client et on quitte la boucle de lecture de la requête
										// Cela permet de traiter plus rapidement une autre requête
										TerminaisonClient();
										continue;
									}
								}
							} else if (strcmp(extension, "jpg") == 0 || strcmp(extension, "jpeg") == 0) {
								printf("TEST17\n");
								if (envoyerReponse200JPG(tableauNomFichier) == -1) {
									printf("TEST18\n");
									// On cloture la connexion avec le client et on quitte la boucle de lecture de la requête
									// Cela permet de traiter plus rapidement une autre requête
									TerminaisonClient();
									continue;
								} else {
									envoyerContenuFichierBinaire(path);
								}
							} else if (strcmp(extension, "js") == 0) {
								printf("TEST JS MAIN 1\n");
								if (envoyerReponse200HTML(tableauNomFichier) == -1) {
									printf("TEST JS MAIN 2\n");
									// On cloture la connexion avec le client et on quitte la boucle de lecture de la requête
									// Cela permet de traiter plus rapidement une autre requête
									TerminaisonClient();
									continue;
								} else {
									if(envoyerContenuFichierTexte(path) == -1){
										printf("TEST JS MAIN 3\n");
										envoyerReponse500HTML("Erreur lors de l'envoi du fichier");
										// On cloture la connexion avec le client et on quitte la boucle de lecture de la requête
										// Cela permet de traiter plus rapidement une autre requête
										TerminaisonClient();
										continue;
									}
								}
							}else if (strcmp(extension, "ico") == 0) {
								printf("TEST ICO MAIN 1\n");
								if (envoyerReponse200ICO(tableauNomFichier) == -1) {
									printf("TEST ICO MAIN 2\n");
									// On cloture la connexion avec le client et on quitte la boucle de lecture de la requête
									// Cela permet de traiter plus rapidement une autre requête
									TerminaisonClient();
									continue;
								} else {
									envoyerContenuFichierBinaire(path);
								}
							} else {
								printf("Tableau Nom Fichier AVANT 404 : %s\n", tableauNomFichier);
								envoyerReponse404HTML(tableauNomFichier);
								TerminaisonClient();
								continue;
							}
						}
					}
				}
			}
			// On purge tout les pointeurs utilisés
			message = NULL;
			tableauNomFichier[255];
			TerminaisonClient();
		}
	}
	return 0;
}