#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    char tableau1[10] = "LA ZONE";
    int tailleTableau = strlen(tableau1);
    printf("Tableau AVANT : %s\n", tableau1);
    test(tableau1, tailleTableau);
    printf("Tableau APRES : %s\n", tableau1);
    return 0;
}

int test(char *tableau, int tailleTableau){
    char subStr[] = "Salut";
    for (size_t i = 0; i < tailleTableau; i++)
    {
        tableau[i] = subStr[i];
    }
    printf("Tableau Fonction : %s\n", tableau);
    return 1;
}
